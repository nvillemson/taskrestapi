package com.project.web.service;

import com.project.persistence.model.Task;
import com.project.persistence.model.User;
import com.project.persistence.repository.TaskCrudRepository;
import com.project.persistence.repository.UserCrudRepository;
import com.project.web.exceptions.TaskNotFoundException;
import com.project.web.exceptions.UserNotFoundException;
import com.project.web.pojo.request.AddUserRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.TaskResponse;
import com.project.web.pojo.responce.UserResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    private UserCrudRepository userCrudRepository;
    private TaskCrudRepository taskCrudRepository;

    @Autowired
    public UserService(UserCrudRepository userCrudRepository, TaskCrudRepository taskCrudRepository) {
        this.userCrudRepository = userCrudRepository;
        this.taskCrudRepository = taskCrudRepository;
    }

    // passing request directly as there is no need to unwrap the request values
    public IdResponse addUser(User user) {
        User saved = userCrudRepository.save(user);
        return new IdResponse(saved.getId());
    }

    public void assignUserTask(Long taskId, Long userId) {
        Optional<Task> task = taskCrudRepository.findById(taskId);
        if (!task.isPresent()) {
            throw new TaskNotFoundException("Task was not found.");
        }
        if (!userCrudRepository.existsById(userId)) {
            throw new UserNotFoundException("User was not found.");
        }
        Task taskActual = task.get();
        userCrudRepository.findById(userId).map(user -> {
            taskActual.setUser(user);
            return taskCrudRepository.save(taskActual);
        });
    }

    public User convertRequestToUser(AddUserRequest request) {
        User user = new User();
        user.setBirthday(request.getBirthday());
        user.setFirstName(request.getFirstName());
        user.setLastName(request.getLastName());
        user.setUserName(request.getUserName());
        user.setPassword(request.getPassword());
        return user;
    }

    public UserResponse findUserWithTasks(Long userId) {
        Optional<User> userById = userCrudRepository.findById(userId);
        if (!userById.isPresent()) {
            throw new UserNotFoundException("User was not found.");
        }
        User user = userById.get();
        UserResponse response = new UserResponse(user.getId(), user.getUserName(), user.getFirstName(), user.getLastName(),
                user.getBirthday(), null);

        List<Task> tasks = user.getTasks();
        List<TaskResponse> taskResponses = new ArrayList<>();
        tasks.forEach(task -> taskResponses.add(new TaskResponse(task.getId(), task.getTaskName(), task.getTaskDescription(),
                task.getDeleted(), task.getUpdated(),
                    task.getCreated(), task.getCompleted())));
        response.setTasks(taskResponses);

        return response;
    }

    public void completeUserTask(Long userId, Long taskId) {
        taskCrudRepository.completeTask(taskId, userId);
    }
}
