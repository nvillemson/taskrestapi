package com.project.web.service;

import com.project.persistence.model.Task;
import com.project.persistence.repository.TaskCrudRepository;
import com.project.web.exceptions.TaskNotFoundException;
import com.project.web.exceptions.TaskUpdateMissingValuesException;
import com.project.web.pojo.request.AddTaskRequest;
import com.project.web.pojo.request.UpdateTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.TaskResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TaskService {

    private TaskCrudRepository taskCrudRepository;

    @Autowired
    public TaskService(TaskCrudRepository taskCrudRepository) {
        this.taskCrudRepository = taskCrudRepository;
    }

    public IdResponse saveTask(Task task) {
        Task saved = taskCrudRepository.save(task);
        return new IdResponse(saved.getId());
    }

    public void removeTask(long taskId) {
        taskCrudRepository.removeTask(taskId);
    }

    public Task convertRequestToTask(AddTaskRequest req) {
        Task task = new Task();
        task.setTaskName(req.getTaskName());
        task.setTaskDescription(req.getTaskDescription());
        return task;
    }

    public TaskResponse searchTaskById(long taskId) {
        Optional<Task> task = taskCrudRepository.findById(taskId);
        if (task.isPresent()) {
            Task actual = task.get();
            return new TaskResponse(actual.getId(), actual.getTaskName(), actual.getTaskDescription(), actual.getDeleted(),
                    actual.getUpdated(), actual.getCreated(), actual.getCompleted());
        } else {
            throw new TaskNotFoundException("Task was not found.");
        }
    }

    public void updateTask(UpdateTaskRequest req) {
        if (req.getTaskDescription() == null && req.getTaskName() == null) {
            throw new TaskUpdateMissingValuesException("Both task description and task name are missing.");
        }

        Optional<Task> task = taskCrudRepository.findById(req.getTaskId());
        if (task.isPresent()) {
            Task actual = task.get();
            if (req.getTaskDescription() != null) {
                actual.setTaskDescription(req.getTaskDescription());
            }
            if (req.getTaskName() != null) {
                actual.setTaskName(req.getTaskName());
            }
            // with spring data jpa save also work as update in case id is filled in
            taskCrudRepository.save(actual);
        } else {
            throw new TaskNotFoundException("Task was not found.");
        }
    }

}
