package com.project.web.controller;

import com.project.persistence.model.User;
import com.project.web.pojo.request.AddUserRequest;
import com.project.web.pojo.request.AssignUserTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.UserResponse;
import com.project.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
@RequestMapping("/user")
public class UserController {

    private UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/add-user")
    public @ResponseBody IdResponse addUser(@RequestBody @Valid AddUserRequest request) {
        User user = userService.convertRequestToUser(request);
        return userService.addUser(user);
    }

    @PostMapping("/assign-user-task")
    @ResponseStatus(HttpStatus.OK)
    public void assignUserTask(@RequestBody @Valid AssignUserTaskRequest request) {
        userService.assignUserTask(request.getTaskId(), request.getUserId());
    }

    @GetMapping("/get-user/{userId}")
    public @ResponseBody UserResponse getUserResponse(@PathVariable("userId") long userId) {
        return userService.findUserWithTasks(userId);
    }

    @PostMapping("/complete-user-task")
    @ResponseStatus(HttpStatus.OK)
    public void completeUserTask(@RequestParam("userId") Long userId, @RequestParam("taskId") Long taskId) {
        userService.completeUserTask(userId, taskId);
    }


}
