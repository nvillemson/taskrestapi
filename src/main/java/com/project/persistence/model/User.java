package com.project.persistence.model;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@SuppressWarnings("unused")
@Entity
@Table(name = "USER")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "USER_NAME", nullable = false, columnDefinition = "VARCHAR(50)")
    private String userName;

    @Column(name = "FIRST_NAME", nullable = false, columnDefinition = "VARCHAR(50)")
    private String firstName;

    @Column(name = "LAST_NAME", nullable = false, columnDefinition = "VARCHAR(50)")
    private String lastName;

    // password actually as to be hashed etc, but for simplicity meaning as a part of assignment
    @Column(name = "PASSWORD", nullable = false, columnDefinition = "VARCHAR(50)")
    private String password;

    @Column(name = "BIRTHDAY", columnDefinition = "DATE", nullable = false)
    private LocalDate birthday;

    @Column(name = "CREATED", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP()")
    private LocalDateTime created;

    @OneToMany(fetch=FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
    private List<Task> tasks;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
