package com.project.persistence.model;

import javax.persistence.*;
import java.time.LocalDateTime;

@SuppressWarnings("unused")
@Entity
@Table(name = "TASK")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TASK_NAME", nullable = false, columnDefinition = "VARCHAR(30)")
    private String taskName;

    @Column(name = "TASK_DESCRIPTION", nullable = false, columnDefinition = "VARCHAR(200)")
    private String taskDescription;

    @Column(name = "DELETED", columnDefinition = "TIMESTAMP")
    private LocalDateTime deleted;

    @Column(name = "UPDATED", columnDefinition = "TIMESTAMP")
    private LocalDateTime updated;

    @Column(name = "CREATED", columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP()")
    private LocalDateTime created;

    @Column(name = "COMPLETED", columnDefinition = "TIMESTAMP")
    private LocalDateTime completed;

    @ManyToOne(fetch=FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name="USER_ID")
    private User user;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public LocalDateTime getDeleted() {
        return deleted;
    }

    public void setDeleted(LocalDateTime deleted) {
        this.deleted = deleted;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    public LocalDateTime getCompleted() {
        return completed;
    }

    public void setCompleted(LocalDateTime completed) {
        this.completed = completed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }
}
