Assignment should compile on Java 8.
I myself used Java 11 for compilation.

It basically runs on spring and java only. Used frameworks -
spring-boot-starter-web
spring-boot-starter-test (for automated tests)
spring-boot-starter-data-jpa (as orm framework)

Tests run on in-memory H2 database.
TaskController and UserController define rest webservice behaviour, correspondingly:
"tasks/add-task";
"tasks/delete-task/";
"tasks/get-task/";
"tasks/update-task";
"user/add-user";
"user/assign-user-task";
"user/get-user/";
"user/complete-user-task";

Database part is in com.project.persistense.
Controller part is in com.project.web.

Code should be simple.
Also there are 11 integration tests based on spring-boot-starter-test.
