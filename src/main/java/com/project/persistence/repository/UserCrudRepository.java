package com.project.persistence.repository;

import com.project.persistence.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserCrudRepository extends CrudRepository<User, Long> {

    @Query("select u from User u left join fetch u.tasks where u.id =:id")
    void selectUserWithTasks(@Param("id") Long id);
}
