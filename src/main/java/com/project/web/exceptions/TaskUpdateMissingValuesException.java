package com.project.web.exceptions;

public class TaskUpdateMissingValuesException extends RuntimeException {

    public TaskUpdateMissingValuesException(String message) {
        super(message);
    }
}
