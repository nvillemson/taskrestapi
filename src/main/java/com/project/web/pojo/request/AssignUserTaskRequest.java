package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;

public class AssignUserTaskRequest {

    @NotNull
    private Long userId;

    @NotNull
    private Long taskId;

    public AssignUserTaskRequest(Long userId, Long taskId) {
        this.userId = userId;
        this.taskId = taskId;
    }

    public AssignUserTaskRequest() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
