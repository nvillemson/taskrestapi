package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.web.pojo.request.AddUserRequest;
import com.project.web.pojo.request.AssignUserTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.TaskResponse;
import com.project.web.pojo.responce.UserResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.LocalDate;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class UserControllerTest extends AbstractTest {

    @Test
    public void testAddUser() throws JsonProcessingException {
        LocalDate birthday = LocalDate.now().minusYears(23);
        AddUserRequest req = new AddUserRequest("Tester", "Tester1", "password", "helloWorld", birthday);
        HttpEntity<String> entity = produceUserEntity(req);
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void testAddUserBadRequest() throws JsonProcessingException {
        // empty input
        AddUserRequest req = new AddUserRequest();
        HttpEntity<String> entity = produceUserEntity(req);
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // partially missing input
        req = new AddUserRequest("Tester", "Tester1", "password", null, null);
        entity = produceUserEntity(req);
        response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // wrong input - too small
        LocalDate birthday = LocalDate.now().minusYears(23);
        req = new AddUserRequest("Tester", "Tester1", "password", "tere", birthday);
        entity = produceUserEntity(req);
        response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void assignTaskTestBadRequest() throws IOException {
        AssignUserTaskRequest request = new AssignUserTaskRequest(111L, 23242L);
        HttpEntity<String> entity = produceTaskAssignEntity(request);
        ResponseEntity<String> response = template.postForEntity(assignsTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Task was not found.", response.getBody());

        IdResponse task = createTask("Hello", "World");
        request = new AssignUserTaskRequest(111L, task.getId());
        entity = produceTaskAssignEntity(request);
        response = template.postForEntity(assignsTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("User was not found.", response.getBody());

        IdResponse user = createUser("Tester2", "Tester2", "password2", "tere2");
        request = new AssignUserTaskRequest(user.getId(), 23242L);
        entity = produceTaskAssignEntity(request);
        response = template.postForEntity(assignsTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Task was not found.", response.getBody());
    }

    @Test
    public void assignTaskTestSuccess() throws IOException {
        IdResponse task = createTask("Hello", "World");
        IdResponse user = createUser("Tester2", "Tester2", "password2", "tere2");
        AssignUserTaskRequest request = new AssignUserTaskRequest(user.getId(), task.getId());
        HttpEntity<String> entity = produceTaskAssignEntity(request);
        ResponseEntity<String> response = template.postForEntity(assignsTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getUserUrl + user.getId(), String.class);
        UserResponse foundUser = mapper.readValue(response.getBody(), UserResponse.class);
        assertEquals(1, foundUser.getTasks().size());
        assertEquals("Tester2", foundUser.getFirstName());
        assertEquals("Tester2", foundUser.getLastName());
        assertEquals("tere2", foundUser.getUserName());
        assertEquals("Hello", foundUser.getTasks().get(0).getTaskName());
        assertEquals("World", foundUser.getTasks().get(0).getTaskDescription());
    }

    @Test
    public void completeUserTaskTestSuccess() throws IOException {
        IdResponse task = createTask("Hello", "World");
        IdResponse user = createUser("Tester2", "Tester2", "password2", "tere2");

        AssignUserTaskRequest request = new AssignUserTaskRequest(user.getId(), task.getId());
        HttpEntity<String> entity = produceTaskAssignEntity(request);
        ResponseEntity<String> response = template.postForEntity(assignsTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.postForEntity(completeUserTaskUrl + "?userId=" + user.getId()
                + "&taskId=" + task.getId(), null, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        String getTaskUrlFull = getTaskUrl + "/" + task.getId();
        response = template.getForEntity(getTaskUrlFull, String.class);
        TaskResponse foundTask = mapper.readValue(response.getBody(), TaskResponse.class);
        assertNotNull(foundTask.getCompleted());
    }

}
