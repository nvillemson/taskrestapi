package com.project.web.pojo.request;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UpdateTaskRequest {

    @Size(min = 5, max = 50)
    private String taskName;

    @Size(min = 5, max = 200)
    private String taskDescription;

    @NotNull
    private Long taskId;

    public UpdateTaskRequest() {
    }

    public UpdateTaskRequest(String taskName, String taskDescription, Long taskId) {
        this.taskName = taskName;
        this.taskDescription = taskDescription;
        this.taskId = taskId;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public String getTaskDescription() {
        return taskDescription;
    }

    public void setTaskDescription(String taskDescription) {
        this.taskDescription = taskDescription;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }
}
