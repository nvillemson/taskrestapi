package com.project.web.controller;

import com.project.persistence.model.Task;
import com.project.web.pojo.request.AddTaskRequest;
import com.project.web.pojo.request.UpdateTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.TaskResponse;
import com.project.web.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@SuppressWarnings("unused")
@Controller
@RequestMapping("/tasks")
public class TaskController {

    private TaskService taskService;

    @Autowired
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    @PostMapping("/add-task")
    public @ResponseBody IdResponse addTask(@RequestBody @Valid AddTaskRequest req) {
        Task task = taskService.convertRequestToTask(req);
        return taskService.saveTask(task);
    }

    @PostMapping("/update-task")
    @ResponseStatus(HttpStatus.OK)
    public void updateTask(@RequestBody @Valid UpdateTaskRequest req) {
        taskService.updateTask(req);
    }

    @PostMapping("/delete-task/{taskId}")
    @ResponseStatus(HttpStatus.OK)
    public void removeFilmFromStock(@PathVariable("taskId") long taskId) {
        taskService.removeTask(taskId);
    }

    @GetMapping("/get-task/{taskId}")
    public @ResponseBody TaskResponse searchTaskById(@PathVariable("taskId") long taskId) {
        return taskService.searchTaskById(taskId);
    }

}
