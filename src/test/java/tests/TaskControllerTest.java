package tests;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.project.web.pojo.request.AddTaskRequest;
import com.project.web.pojo.request.UpdateTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import com.project.web.pojo.responce.TaskResponse;
import general.AbstractTest;
import org.junit.Test;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class TaskControllerTest extends AbstractTest {

    @Test
    public void testAddTask() throws JsonProcessingException {
        AddTaskRequest req = new AddTaskRequest("Task1", "Sample Task Description");
        HttpEntity<String> entity = produceTaskEntity(req);
        ResponseEntity<String> response = template.postForEntity(addTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    @Test
    public void testAddTaskBadRequest() throws JsonProcessingException {
        // empty invalid request
        AddTaskRequest req = new AddTaskRequest(null, null);
        HttpEntity<String> entity = produceTaskEntity(req);
        ResponseEntity<String> response = template.postForEntity(addTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

        // wrong input data
        req = new AddTaskRequest("t", "t2");
        entity = produceTaskEntity(req);
        response = template.postForEntity(addTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
    }

    @Test
    public void testDeleteTask() throws IOException {
        IdResponse createdTask = createTask("Task1", "Sample Task Description");

        String getTaskUrlFull = getTaskUrl + "/" + createdTask.getId();
        ResponseEntity<String> response = template.getForEntity(getTaskUrlFull, String.class);
        TaskResponse foundTask = mapper.readValue(response.getBody(), TaskResponse.class);
        assertNull(foundTask.getDeleted());

        response = template.postForEntity(deleteTaskUrl + createdTask.getId(), null, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        response = template.getForEntity(getTaskUrl + createdTask.getId(), String.class);
        foundTask = mapper.readValue(response.getBody(), TaskResponse.class);
        assertNotNull(foundTask.getDeleted());
    }

    @Test
    public void testSearchTaskNotFound() {
        String getTaskUrlFull = getTaskUrl + "/5467347";
        ResponseEntity<String> response = template.getForEntity(getTaskUrlFull, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Task was not found.", response.getBody());
    }

    @Test
    public void testUpdateTaskBadRequest() throws IOException {
        // not existing task
        UpdateTaskRequest req = new UpdateTaskRequest("testName", "TestId", 3547L);
        HttpEntity<String> entity = produceTaskUpdateEntity(req);
        ResponseEntity<String> response = template.postForEntity(updateTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Task was not found.", response.getBody());

        // missing data in request
        IdResponse task = createTask("Task1", "Sample Task Description");

        req = new UpdateTaskRequest(null, null, task.getId());
        entity = produceTaskUpdateEntity(req);
        response = template.postForEntity(updateTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));
        assertEquals("Both task description and task name are missing.", response.getBody());
    }

    @Test
    public void testUpdateTask() throws IOException {
        IdResponse task = createTask("Task2", "Sample Task Description2");

        UpdateTaskRequest req = new UpdateTaskRequest("testName", "Test description", task.getId());
        HttpEntity<String> entity = produceTaskUpdateEntity(req);
        ResponseEntity<String> response = template.postForEntity(updateTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));

        String getTaskUrlFull = getTaskUrl + "/" + task.getId();
        response = template.getForEntity(getTaskUrlFull, String.class);
        TaskResponse foundTask = mapper.readValue(response.getBody(), TaskResponse.class);
        assertEquals("testName", foundTask.getTaskName());
        assertEquals("Test description", foundTask.getTaskDescription());
    }

}
