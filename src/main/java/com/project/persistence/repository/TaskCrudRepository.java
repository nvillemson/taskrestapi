package com.project.persistence.repository;

import com.project.persistence.model.Task;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface TaskCrudRepository extends CrudRepository<Task, Long> {

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query("UPDATE Task t SET t.deleted = CURRENT_TIMESTAMP() WHERE t.id = ?1")
    void removeTask(long taskId);

    @Modifying
    @Transactional(Transactional.TxType.REQUIRED)
    @Query(value = "UPDATE TASK t SET t.COMPLETED = CURRENT_TIMESTAMP() WHERE t.ID = :taskId AND t.USER_ID = :userId", nativeQuery = true)
    void completeTask(@Param("taskId") Long taskId, @Param("userId") Long userId);



}
