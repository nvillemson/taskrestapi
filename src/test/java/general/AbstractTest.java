package general;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.Application;
import com.project.web.pojo.request.AddTaskRequest;
import com.project.web.pojo.request.AddUserRequest;
import com.project.web.pojo.request.AssignUserTaskRequest;
import com.project.web.pojo.request.UpdateTaskRequest;
import com.project.web.pojo.responce.IdResponse;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class}, webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public abstract class AbstractTest {

    @LocalServerPort
    private int port;

    protected ObjectMapper mapper;

    protected String addUserUrl;
    protected String addTaskUrl;
    protected String deleteTaskUrl;
    protected String getTaskUrl;
    protected String updateTaskUrl;
    protected String assignsTaskUrl;
    protected String getUserUrl;
    protected String completeUserTaskUrl;

    @Autowired
    protected TestRestTemplate template;

    @Before
    public void setUp() throws Exception {
        URL base = new URL("http://localhost:" + port + "/");
        addUserUrl = base.toString() + "user/add-user";
        addTaskUrl = base.toString() + "tasks/add-task";
        deleteTaskUrl = base.toString() + "tasks/delete-task/";
        getTaskUrl = base.toString() + "tasks/get-task/";
        updateTaskUrl = base.toString() + "tasks/update-task";
        assignsTaskUrl = base.toString() + "user/assign-user-task";
        getUserUrl = base.toString() + "user/get-user/";
        completeUserTaskUrl = base.toString() + "user/complete-user-task";
        mapper = new ObjectMapper();
        mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    protected HttpEntity<String> produceUserEntity(AddUserRequest addFilmRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(addFilmRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected IdResponse createUser(String firstName, String lastName, String password, String userName) throws IOException {
        LocalDate birthday = LocalDate.now().plusYears(27);
        AddUserRequest request = new AddUserRequest(firstName, lastName, password, userName, birthday);
        HttpEntity<String> entity = produceUserEntity(request);
        ResponseEntity<String> response = template.postForEntity(addUserUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        return mapper.readValue(response.getBody(), IdResponse.class);
    }

    protected IdResponse createTask(String taskName, String taskDescription) throws IOException {
        AddTaskRequest request = new AddTaskRequest(taskName, taskDescription);
        HttpEntity<String> entity = produceTaskEntity(request);
        ResponseEntity<String> response = template.postForEntity(addTaskUrl, entity, String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        return mapper.readValue(response.getBody(), IdResponse.class);
    }

    protected HttpEntity<String> produceTaskEntity(AddTaskRequest addTaskRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(addTaskRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected HttpEntity<String> produceTaskUpdateEntity(UpdateTaskRequest updateTaskRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(updateTaskRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }

    protected HttpEntity<String> produceTaskAssignEntity(AssignUserTaskRequest assignUserTaskRequest) throws JsonProcessingException {
        String json = mapper.writeValueAsString(assignUserTaskRequest);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        return new HttpEntity<>(json, headers);
    }
}
